package com.leanxcale.example.jdbcLowLevel;

import com.leanxcale.example.AbstractTestExecution;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicInteger;
import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;

@Slf4j
public abstract class AbstractJdbcTestExecution extends AbstractTestExecution {

  private static final AtomicInteger counter = new AtomicInteger();

  @Value("${jdbc.lx.url}")
  private String lxUrl;
  @Value("${jdbc.lx.user}")
  private String lxUser;
  @Value("${jdbc.lx.password}")
  private String lxPwd;

  private String[] lxUrls;

  @PostConstruct
  public void initQEs() {
    lxUrls = lxUrl.split(",");
  }

  // This emulates Round Robin to connect to all Query Engine instances
  protected Connection connect() throws SQLException {
    int id = counter.getAndIncrement();
    String url = lxUrls[id%lxUrls.length];
    int start = url.indexOf("//")+2;
    String u = url.substring(start, url.indexOf("/", start));
    log.info("Connecting to lx:{}", u);
    Connection conn = DriverManager.getConnection(url, lxUser, lxPwd);
    conn.setAutoCommit(false);
    return conn;
  }


  @Override
  protected void execute(String sql) throws SQLException {
    try (Connection connection = connect()){
      connection.createStatement().executeUpdate(sql);
      connection.commit();
    }
  }
}
