package com.leanxcale.example.spring;

import com.leanxcale.example.spring.config.SpringConfig;
import com.leanxcale.example.data.LoansReaderService;
import com.leanxcale.example.stats.StatsPrinter;
import com.leanxcale.exception.LeanxcaleException;
import com.leanxcale.kivi.database.Table;
import com.leanxcale.kivi.session.Session;
import com.leanxcale.kivi.session.SessionFactory;
import com.leanxcale.kivi.session.Settings;
import com.leanxcale.kivi.tuple.Tuple;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test uses the direct kivi-api instead of jdbc to add data.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes= SpringConfig.class)
@Slf4j
public class Test11_Spring_NativeKiviApiImproved extends AbstractSpringTestExecution {

  @Resource(name= "lxPermanentJdbcTemplate")
  JdbcTemplate lxJdbcTemplate;  //just for creating tables with parent class

  @Autowired
  LoansReaderService loansReader;

    @Autowired
  StatsPrinter stats;


  private int duration;

  @Value("${multithreading.concurrency}")
  private int N_THREADS;

  @Value("${batch.size}")
  private int BATCH_SIZE;
  private Thread mainThread;

  @Value("${kivi.kx.url}")
  private String kiviUrl;

  @Before
  public void init() throws IOException, URISyntaxException, SQLException {
    super.initPkRangeAutosplitDb();
  }

  @Test
  public void testLx() throws InterruptedException, LeanxcaleException {
    test();
  }

  private void test() throws InterruptedException, LeanxcaleException {
    Settings settings = Settings.parse(kiviUrl);
    stats.start();

    IntStream.range(0, N_THREADS).forEach(x -> {
      new Thread(() -> {
        log.info("Running test thread:{}", Thread.currentThread().getName());
        try {
          doTest(settings,x);

        } catch (Exception e) {
          throw new RuntimeException(e);
        }
      }, "TestThread-" + x).start();
    });

    log.info("Waiting for test ending. Duration: [{}] ms", duration);
    this.mainThread = Thread.currentThread();
    try{
      Thread.sleep(duration);
    }catch (InterruptedException e){
      log.trace("Thread interrupted", e);
    }
    log.info("Ending");
  }

  private void doTest(Settings settings, int threadId) throws IOException, LeanxcaleException, URISyntaxException {
    try(Session session = SessionFactory.newSession(settings)){
      Table table = session.database().getTable(TABLE_NAME);
      Tuple tuple = table.createTuple();
      final AtomicInteger i = new AtomicInteger(0);

      String fieldsArray[] = new String[] {
          "loan_id", "client_id", "ts", "issue_d", "loan_status", "loan_amnt", "term", "int_rate", "installment", "grade", "emp_length",
          "home_ownership", "annual_inc", "verification_status", "mths_since_issue_d", "desc", "purpose", "title", "zip_code", "addr_state",
          "dti", "mths_since_earliest_cr_line", "inq_last_6mths", "revol_util", "out_prncp", "total_pymnt", "total_rec_int",
          "last_pymnt_d", "mths_since_last_pymnt_d", "mths_since_last_credit_pull_d", "total_rev_hi_lim"
      };
      int[] fieldsIndexes = table.getFieldsIndexes(fieldsArray);
      Object[] values = new Object[fieldsArray.length];

      while (true) {//hack to run n-times over the same csv file
        this.loansReader.readData(threadId, N_THREADS).forEach(loan -> {

          values[fieldsIndexes[0]] = loan.getId();
          values[fieldsIndexes[1]] = loan.getClientId();
          values[fieldsIndexes[2]] = loan.getTs();
          values[fieldsIndexes[3]] = loan.getIssue_d();
          values[fieldsIndexes[4]] = loan.getLoan_status();
          values[fieldsIndexes[5]] = loan.getLoan_amnt();
          values[fieldsIndexes[6]] = loan.getTerm();
          values[fieldsIndexes[7]] = loan.getInt_rate();
          values[fieldsIndexes[8]] = loan.getInstallment();
          values[fieldsIndexes[9]] = loan.getGrade();
          values[fieldsIndexes[10]] = loan.getEmp_length();
          values[fieldsIndexes[11]] = loan.getHome_ownership();
          values[fieldsIndexes[12]] = loan.getAnnual_inc();
          values[fieldsIndexes[13]] = loan.getVerification_status();
          values[fieldsIndexes[14]] = loan.getMths_since_issue_d();
          values[fieldsIndexes[15]] = loan.getDesc();
          values[fieldsIndexes[16]] = loan.getPurpose();
          values[fieldsIndexes[17]] = loan.getTitle();
          values[fieldsIndexes[18]] = loan.getZip_code();
          values[fieldsIndexes[19]] = loan.getAddr_state();
          values[fieldsIndexes[20]] = loan.getDti();
          values[fieldsIndexes[21]] = loan.getMths_since_earliest_cr_line();
          values[fieldsIndexes[22]] = loan.getInq_last_6mths();
          values[fieldsIndexes[23]] = loan.getRevol_util();
          values[fieldsIndexes[24]] = loan.getOut_prncp();
          values[fieldsIndexes[25]] = loan.getTotal_pymnt();
          values[fieldsIndexes[26]] = loan.getTotal_rec_int();
          values[fieldsIndexes[27]] = loan.getLast_pymnt_d();
          values[fieldsIndexes[28]] = loan.getMths_since_last_pymnt_d();
          values[fieldsIndexes[29]] = loan.getMths_since_last_credit_pull_d();
          values[fieldsIndexes[30]] = loan.getTotal_rev_hi_lim();

          tuple.putAll(values);

          if (isInsert){
            table.insert(tuple);
          }else{
            table.upsert(tuple);
          }
          if (i.incrementAndGet() % BATCH_SIZE == 0) {
            session.commit();
            i.set(0);
            boolean limitReached = stats.incrementInsert(BATCH_SIZE);
            if (limitReached && duration == Long.MAX_VALUE){ //duration set to -1. exit when limit is reached
              log.info("Limit of rows for test reached. Ending test");
              this.mainThread.interrupt();
            }          }
        });

      }
    }

  }



  @Override
  protected JdbcTemplate getLxJdbcTemplate() {
    return lxJdbcTemplate;
  }

}
