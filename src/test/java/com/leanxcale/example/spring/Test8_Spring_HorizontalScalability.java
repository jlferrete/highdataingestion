package com.leanxcale.example.spring;

import com.leanxcale.example.data.LoansReaderService;
import com.leanxcale.example.spring.config.SpringConfig;
import com.leanxcale.example.stats.StatsPrinter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test is exactly the same as Test7_Autosplit but should be run agains a scaled out LX
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes= SpringConfig.class)
@Slf4j
public class Test8_Spring_HorizontalScalability extends AbstractSpringTestExecution {
  private static final int MAX_RETRIES = 3;

  @Resource(name="lxPoolJdbcTemplate")
  JdbcTemplate lxJdbcTemplate;


  @Autowired
  LoansReaderService loansReader;

  @Autowired
  StatsPrinter stats;

  @Value("${multithreading.concurrency}")
  private int N_THREADS;

  @Value("${batch.size}")
  private int BATCH_SIZE;

  private Thread mainThread;

  @Before
  public void init() throws IOException, URISyntaxException, SQLException {
    super.initHashAutosplitDb();

  }

  @Test
  public void testLx() throws SQLException, InterruptedException {
    test();
  }


  private void test() throws InterruptedException {
    stats.start();
    IntStream.range(0, N_THREADS).forEach(x -> {
      new Thread(() -> {
        log.info("Running test thread:{}", Thread.currentThread().getName());
        try {
          doTest(x);

        } catch (Exception e) {
          throw new RuntimeException(e);
        }
      }, "TestThread-" + x).start();
    });

    log.info("Waiting for test ending. Duration: [{}] ms", duration);
    this.mainThread = Thread.currentThread();
    try{
      Thread.sleep(duration);
    }catch (InterruptedException e){
      log.trace("Thread interrupted", e);
    }
    log.info("Ending");
  }

  private void doTest(int threadId) throws SQLException, IOException, URISyntaxException {
    String sql =
        insertCommand + " into loans(loan_id, client_id , ts , issue_d,loan_status, loan_amnt , term , int_rate " +
            ", installment , grade , emp_length , home_ownership" +
            ", annual_inc , verification_status , mths_since_issue_d" +
            ", \"desc\" , purpose , title , zip_code , addr_state" +
            ", dti , mths_since_earliest_cr_line , inq_last_6mths " +
            ", revol_util , out_prncp , total_pymnt , total_rec_int " +
            ", last_pymnt_d , mths_since_last_pymnt_d , mths_since_last_credit_pull_d, total_rev_hi_lim" +
            ") values (?, ? ,? ,? ,?, ?, ? ,? ,? ,?, ?, ? ,? ,? ,?, ?, ? ,? ,? ,?, ?, ? ,? ,? ,?, ?, ? ,? ,? ,?, ?) ";

    while (true) {//hack to run n-times over the same csv file
      List<Object[]> batchArgs = new ArrayList<>(BATCH_SIZE);
      final AtomicInteger i = new AtomicInteger(0);
      this.loansReader.readData(threadId, N_THREADS).forEach(loan -> {

        Object[] args =
            new Object[]{loan.getId(), loan.getClientId(), loan.getTs(), loan.getIssue_d(), loan.getLoan_status(), loan.getLoan_amnt(),
                loan.getTerm(), loan.getInt_rate(), loan.getInstallment(), loan.getGrade(), loan.getEmp_length(), loan.getHome_ownership(), loan.getAnnual_inc(), loan.getVerification_status(),
                loan.getMths_since_issue_d(), loan.getDesc(), loan.getPurpose(), loan.getTitle(), loan.getZip_code(),
                loan.getAddr_state(), loan.getDti(), loan.getMths_since_earliest_cr_line(), loan.getInq_last_6mths(),
                loan.getRevol_util(), loan.getOut_prncp(), loan.getTotal_pymnt(), loan.getTotal_rec_int(), loan.getLast_pymnt_d(),
                loan.getMths_since_last_pymnt_d(), loan.getMths_since_last_credit_pull_d(), loan.getTotal_rev_hi_lim()};

        batchArgs.add(args);
        if (i.incrementAndGet() % BATCH_SIZE == 0) {
          batchInsert(sql, batchArgs, 0);
          i.set(0);
          batchArgs.clear();
          boolean limitReached = stats.incrementInsert(BATCH_SIZE);
          if (limitReached && duration == Long.MAX_VALUE){ //duration set to -1. exit when limit is reached
            log.info("Limit of rows for test reached. Ending test");
            this.mainThread.interrupt();
          }
        }
      });
    }
  }

  private void batchInsert(String sql, List<Object[]> batchArgs, int retry) {
    try{
      lxJdbcTemplate.batchUpdate(sql, batchArgs);
    }catch (Exception e) {
      if (retry < MAX_RETRIES) {
        log.warn("Error inserting bath. Trying to retry. attempt: [{}/{}]. Error msg:[{}]", retry+1, MAX_RETRIES, e.getMessage()); //FIXME llevar a otros tests
        batchInsert(sql, batchArgs, retry+1); //retry insert
      }else{
        log.error("Error inserting bath. Max retries attempts reached:[{}]", MAX_RETRIES, e );
      }
    }
  }


  @Override
  protected JdbcTemplate getLxJdbcTemplate() {
    return lxJdbcTemplate;
  }

}
