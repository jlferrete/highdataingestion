package com.leanxcale.example.avl;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Represents the PK of the table. Used just to calculate the split points
 */
@Data
@AllArgsConstructor
public class LoanAvlKey implements Comparable<LoanAvlKey>{

  private long clientId;
  private long loanId;

  @Override
  public int compareTo(LoanAvlKey o) {
    if (this.clientId > o.clientId) return 1;
    if (this.clientId < o.clientId) return -1;
    //clientId equals
    if (this.loanId > o.loanId) return 1;
    if (this.loanId < o.loanId) return -1;
    return 0;
  }
}
