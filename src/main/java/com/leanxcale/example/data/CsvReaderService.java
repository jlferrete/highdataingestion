package com.leanxcale.example.data;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class CsvReaderService {

  @Value("${csvreader.separator}")
  private String SEPARATOR;

  /** Read a file as a Stream of lines already split by the csv separator */
  public Stream<String[]> readCsvFile(String file) throws IOException, URISyntaxException {
    log.info("Reading data from file:{}", file);
    return Files.lines(Paths.get(getClass().getClassLoader().getResource(file).toURI())).map(x->{return x.split(SEPARATOR);});
  }


  public Stream<String[]> readPartialCsvFile(String file, int section, int totalSections) throws IOException, URISyntaxException {
    log.debug("Reading partial data from file:{}. Section [{}/{}]", file, section, totalSections-1);

    RandomAccessFile randomAccessFile = new RandomAccessFile(new File(getClass().getClassLoader().getResource(file).toURI()), "r");
    long length = randomAccessFile.length();
    long sectionSize = (int) Math.floor(length / totalSections); //divide the file in n equitable parts
    long sectionPos = section * sectionSize;
    long nextSectionPos = (section+1) * sectionSize;

    if (section > 0) {
      randomAccessFile.seek(sectionPos); //go to the start of the section.
      randomAccessFile.readLine(); //discard this partial line (will be read by the previous section thread) and goes to the start of the next one
    }

    log.debug("Reading section [{}/{}] from byte [{}] to byte[{}]", section, totalSections-1, randomAccessFile.getFilePointer(), nextSectionPos);

    Stream<String> stream = StreamSupport.stream(new Spliterator<String>() {

      @SneakyThrows
      @Override
      public boolean tryAdvance(Consumer<? super String> action) {
        try{
          if (randomAccessFile.getFilePointer() <= nextSectionPos) { //pointer is still inside its section
            String line = randomAccessFile.readLine();
            if (line == null) { //end of file
              log.debug("End of file reached");
              randomAccessFile.close();
              return false;
            }

            action.accept(line);
            return true;
          }
          log.debug("End of section [{}/{}] reached", section, totalSections - 1);
          randomAccessFile.close();
          return false;
        } catch (Exception e) {
          try {
            randomAccessFile.close();
          } catch (IOException ex) {
            throw new RuntimeException(ex);
          }
          throw new RuntimeException(e);
        }
      }

      @Override
      public Spliterator<String> trySplit() {
        return null;
      }

      @Override
      public long estimateSize() {
        return sectionSize;
      }

      @Override
      public int characteristics() {
        return Spliterator.IMMUTABLE;
      }
    }, false);

    return stream.map(x->{return x.split(SEPARATOR);});

  }

  public Stream<String[]> readRandomPercentage(String file, int percentage) throws IOException, URISyntaxException {
    log.info("Reading randomly a {}% from file:{}", percentage, file);
    FileRandomReadSpliterator fileSpliterator = new FileRandomReadSpliterator(file, 5);
    Stream<String> stream = StreamSupport.stream(fileSpliterator, true);
    return stream.map(x->{return x.split(SEPARATOR);});
  }




}

